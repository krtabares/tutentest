import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from  '../../services/auth.service';
import { TabsPage } from '../tabs/tabs'
import { AlertController, LoadingController  } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	user: string;
	pass: string;
	isLogged: boolean;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	private auth:AuthService,
  	public loadingCtrl: LoadingController,
  	private alertCtrl: AlertController
  	 ) {
  }

	Signup():void {
		
		let f = {user: this.user, pass: this.pass};
		
		this.presentLoading();
		this.auth.login(this.user, this.pass)
			.subscribe(
				rs => this.isLogged = rs,
				er => {console.log(er); this.showAlert(er._body);  },
				() =>{
					 
					if(this.isLogged){
						this.navCtrl.setRoot(TabsPage)
						.then(data => console.log(data),
							error=> console.log(error));
					}else{
						console.log('Acceso denegado');
					}
				}
			)
	}

showAlert(msj) {
    let alert = this.alertCtrl.create({
			    title: 'Error',
			    subTitle: msj,
			    buttons: ['Cerrar']
			  });
    alert.present();
}
   presentLoading() {
	    this.loadingCtrl.create({
	      content: 'Por Favor Espere...',
	      duration: 3000,
	      dismissOnPageChange: true
	    }).present();
	}
}
