import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
   userName: string;
   loggedIn: boolean;
   url = 'https://dev.tuten.cl:443/TutenREST/rest/user';

   constructor(private http: Http) {
      this.userName = '';
      this.loggedIn = false;
   }
   
   login(user, pass) {
      let url = `${this.url}/${user}` ;
      console.log(pass)
      let userInfo = {};
      let iJon = JSON.stringify(userInfo);

   
      return this.http.put(url, iJon, {
         headers: new Headers({
            'Content-Type':'application/json',
            'password': pass,
            'app': 'APP_WEB'
            
         })
      })
      .map(res => res.text())
      .map(res => {
         if (res=="error" || res=="nofound"){
            this.loggedIn = false;
         } else {
            localStorage.setItem('token', res);
            localStorage.setItem('user', JSON.stringify(res));
            this.userName = userInfo.user;
            this.loggedIn = true;
         }
         return this.loggedIn;
      });
   }

   logout(): void {
      localStorage.removeItem('token');
      this.userName = '';
      this.loggedIn = false;
   }

   isLoggedIn() {
      return this.loggedIn;
   }

   getUser() {
        return JSON.parse(localStorage.getItem('user'))
   }
}